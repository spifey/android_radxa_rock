#include <stdio.h>

#include <cutils/properties.h>
#define LOG_TAG "bluetooth_helper"
#include <cutils/log.h>

#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>


static int rfkill_id = -1;
static char *rfkill_state_path = NULL;

static int init_rfkill() {
    char path[64];
    char buf[16];
    int fd;
    int sz;
    int id;
    for (id = 0; ; id++) {
        snprintf(path, sizeof(path), "/sys/class/rfkill/rfkill%d/type", id);
        fd = open(path, O_RDONLY);
        if (fd < 0) {
            printf("open(%s) failed: %s (%d)\n", path, strerror(errno), errno);
            return -1;
        }
        sz = read(fd, &buf, sizeof(buf));
        close(fd);
        if (sz >= 9 && memcmp(buf, "bluetooth", 9) == 0) {
            rfkill_id = id;
            break;
        }
    }

    asprintf(&rfkill_state_path, "/sys/class/rfkill/rfkill%d/state", rfkill_id);
    return 0;
}

// cmy@20121128: get bt module name from rfkill
int bt_get_chipname(char* name, int len)
{
    int ret = -1;
    int fd = -1;
    int sz = 0;
    char* rfkill_name_path = NULL;
    
    if (rfkill_id == -1) {
        if (init_rfkill()) goto out;
    }

    asprintf(&rfkill_name_path, "/sys/class/rfkill/rfkill%d/name", rfkill_id);

    fd = open(rfkill_name_path, O_RDONLY);
    if (fd < 0) {
        printf("open(%s) failed: %s (%d)", rfkill_name_path, strerror(errno),
             errno);
        goto out;
    }
    
    sz = read(fd, name, len);
    if (sz <= 0) {
        printf("read(%s) failed: %s (%d)", rfkill_name_path, strerror(errno),
             errno);
        goto out;
    }
    name[sz] = '\0';
    if (name[sz-1]=='\n')
        name[sz-1] = '\0';

    ret = 0;

out:
    if (fd >= 0) close(fd);
    return ret;
}

static void ba2str(char* bdaddr, char* bdaddr_str)
{
    sprintf(bdaddr_str, "%02X:%02X:%02X:%02X:%02X:%02X",
            bdaddr[0], bdaddr[1], bdaddr[2],
            bdaddr[3], bdaddr[4], bdaddr[5]);
}

static void str2ba(char* bdaddr_str, char* bdaddr)
{
    sscanf(bdaddr_str, "%02X:%02X:%02X:%02X:%02X:%02X",
            &bdaddr[0], &bdaddr[1], &bdaddr[2],
            &bdaddr[3], &bdaddr[4], &bdaddr[5]);
}

#define READ_BDADDR_FROM_FLASH  0x01
#define BT_ADDRESS_FILE "/data/misc/bluetoothd/bt_addr"

static int read_saved_bdaddr(char *addr)
{
    int fd;

    fd = open(BT_ADDRESS_FILE, O_RDONLY);
    if (fd < 0) {
        printf("Cannot open \"%s\": %s\n", BT_ADDRESS_FILE, strerror(errno));
        return -1;
    }

    read(fd, addr, 18);
    close(fd);
    return 0;
}

static void write_bdaddr(char *bd_addr)
{
    int fd;
    char bd_addr_str[18] = {'\0'};

    ba2str(bd_addr, bd_addr_str);

    fd = open(BT_ADDRESS_FILE, O_CREAT|O_RDWR, 0666);
    if (fd < 0) {
        printf("Cannot create \"%s\": %s\n", BT_ADDRESS_FILE, strerror(errno));
        return;
    }

    write(fd, bd_addr_str, 18);
    close(fd);

    chmod(BT_ADDRESS_FILE, 0666);
}

static int read_btaddr_from_vflash(char* bd_addr)
{
    int fd;
    char bdaddr_vflash[6];
    char bd_addr_str[20] = {0};

    fd = open("/dev/vflash", O_RDWR|O_EXCL);
    if(fd < 0)
    {
        printf("Can't open vflash! fd=0x%x\n", fd);
        return -1;
    }

    if(ioctl(fd, READ_BDADDR_FROM_FLASH, (unsigned long)bdaddr_vflash) < 0)
    {
        printf("vflash ioctl failed\n");
        close(fd);
        return -1;
    }

    close(fd);

    bd_addr[0] = bdaddr_vflash[5];
    bd_addr[1] = bdaddr_vflash[4];
    bd_addr[2] = bdaddr_vflash[3];
    bd_addr[3] = bdaddr_vflash[2];
    bd_addr[4] = bdaddr_vflash[1];
    bd_addr[5] = bdaddr_vflash[0];

    if((bd_addr[0] == 0) &&
        (bd_addr[1] == 0) &&
        (bd_addr[2] == 0) &&
        (bd_addr[3] == 0) &&
        (bd_addr[4] == 0) &&
        (bd_addr[5] == 0))
    {
        return -1;
    }

    //write_saved_bdaddr(bd_addr);
    ba2str(bd_addr, bd_addr_str);
    printf("Read default bdaddr of %s from vflash\n", bd_addr_str);

    return 0;
}

static int read_btaddr_from_file(char* bd_addr)
{
    int i;
    char bd_addr_str[20] = {0};

    //we get a address from bt_addr file
    if(read_saved_bdaddr(bd_addr_str) < 0)
    {
        return -1;
    }
    
    printf("Read default bdaddr of %s at bt_addr file\n", bd_addr_str);

    str2ba(bd_addr_str, bd_addr);

    return 0;
}

void make_random_bdaddr(char * bd_addr)
{
    int i=0;
    char bd_addr_str[20] = {0};
    
    // generate a random address
    srand((int)time(0));

    for(i=0; i<6; i++)
    {
        bd_addr[i] = rand()%255;
    }

    ba2str(bd_addr, bd_addr_str);
    printf("Read default bdaddr of %s first at bt_addr file\n", bd_addr_str);
}

void process_bdaddr()
{
    int ret = 0;
    char bd_addr[6] = {0};
    if (read_btaddr_from_vflash(bd_addr)==0) {
        // write to file
        write_bdaddr(bd_addr);
    } else if (read_btaddr_from_file(bd_addr)==0) {
    } else {
        // make an radom bt addr
        make_random_bdaddr(bd_addr);
        // write to file
        write_bdaddr(bd_addr);
    }
}

int main(int argc, char** argv)
{
    int parm;
    char bt_cname[128];

    while ((parm = getopt(argc,argv,"dc"))!= -1)
    {
        switch (parm) {
        case 'd':// debug
            break;
        default:
            exit(-1);
        }
    }

    process_bdaddr();

    init_rfkill();

    bt_get_chipname(bt_cname, 127);

    printf("BT Chip name: %s\n", bt_cname);

    if (!strcmp(bt_cname, "ap6476")) {
        if (property_set("ctl.start", "gpsd") < 0) {
            printf("Failed to start gps daemon");
        }
    }
    
    return 0;
}


LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := eng

LOCAL_SRC_FILES := $(call all-subdir-java-files)

LOCAL_PACKAGE_NAME := leexplorer

LOCAL_JAVA_LIBRARIES := com.broadcom.bt

include $(BUILD_PACKAGE)
include $(call all-makefiles-under,$(LOCAL_PATH))

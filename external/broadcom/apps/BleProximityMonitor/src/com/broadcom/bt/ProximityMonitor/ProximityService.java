/************************************************************************************
 *
 *  Copyright (C) 2009-2011 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ************************************************************************************/
package com.broadcom.bt.ProximityMonitor;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.ParcelUuid;
import android.os.Parcelable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.Log;

import com.broadcom.bt.gatt.BluetoothGatt;
import com.broadcom.bt.gatt.BluetoothGattAdapter;
import com.broadcom.bt.gatt.BluetoothGattCallback;
import com.broadcom.bt.gatt.BluetoothGattCharacteristic;
import com.broadcom.bt.gatt.BluetoothGattDescriptor;
import com.broadcom.bt.gatt.BluetoothGattService;
import android.bluetooth.BluetoothProfile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class ProximityService extends Service {

    public static final String PROXIMITY_INSERT = "com.broadcom.bt.proximityservice.insert";
    public static final String PROXIMITY_UPDATE = "com.broadcom.bt.proximityservice.update";
    public static final String PROXIMITY_DELETE = "com.broadcom.bt.proximityservice.remove";
    public static final String PROXIMITY_CONNECT = "com.broadcom.bt.proximityservice.connect";
    public static final String PROXIMITY_DISCONNECT = "com.broadcom.bt.proximityservice.disconnect";

    public static final String LE_DEVICE_FOUND = "com.broadcom.gatt.le_device_found";
    public static final String EXTRA_DEVICE = "DEVICE";

    private static final UUID LINK_LOSS_SERVICE_UUID = UUID.fromString("00001803-0000-1000-8000-00805f9b34fb");
    private static final UUID ALERT_LEVEL_CHARACTERISTIC_UUID = UUID.fromString("00002a06-0000-1000-8000-00805f9b34fb");
    private static final UUID CLIENT_CONFIGURATION_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private static final UUID IMMEDIATE_ALERT_SERVICE_UUID = UUID.fromString("00001802-0000-1000-8000-00805f9b34fb");
    
    static ProximityService instance = null;
    static final String TAG = "ProximityService"; 

    private BluetoothGatt mBluetoothGatt = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private ArrayList<BluetoothDevice> mScannedLEDevices = new ArrayList<BluetoothDevice>();
    private ArrayList<BluetoothDevice> mConnectedDevices = new ArrayList<BluetoothDevice>();
    
    private UUID[] scanUUIDs = {LINK_LOSS_SERVICE_UUID,IMMEDIATE_ALERT_SERVICE_UUID};

    LocationManager locationManager;
    private boolean updatingLocation = false;
    private boolean mScanBackGroundFlag = false;
    private LocationListener locationListener;
    private boolean mDiscovered = false;
    

    

    public class LocalBinder extends Binder {
        ProximityService getService() {
            return ProximityService.this;
        }
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return binder;
    }

    private final IBinder binder = new LocalBinder();

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {                
                    BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    Log.d(TAG, "... mBroadcastReceiver.onReceive() device.address=="+device.getAddress());
                    if (device != null) {
                    int bondState = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.BOND_NONE);
                    Log.d(TAG, "... bonded: " + device.getAddress() + " / " + bondState);
                    if (bondState == BluetoothDevice.BOND_NONE) {
                        if (ProximityReporter.containsKey(device.getAddress())) {
                            Log.d(TAG, "Device " + device.getAddress() + " unpaired - cancelling background connection" );
                            remove(device, true);
                        }
                    } else if (bondState == BluetoothDevice.BOND_BONDED) {
                        Log.d(TAG, "Scanning for LE UUIDs " + device.getAddress());                   
                       
                    }
                }
              }
            
    };    
    
    /**
     * Profile service connection listener
     */
    private BluetoothProfile.ServiceListener mProfileServiceListener =
            new BluetoothProfile.ServiceListener() {
            public void onServiceConnected(int profile, BluetoothProfile proxy) {
                Log.d(TAG, "mProfileServiceListener - onServiceConnected");
                mBluetoothGatt = (BluetoothGatt) proxy;
                mBluetoothGatt.registerApp(mGattCallbacks);
            }

            public void onServiceDisconnected(int profile) {
                Log.d(TAG, "mProfileServiceListener - onServiceDisconnected");
                mBluetoothGatt = null;
            }
        };   
    
    /**
     * GATT client callbacks
     */
    private BluetoothGattCallback mGattCallbacks = new BluetoothGattCallback() {
        
        @Override
        public void onConnectionStateChange(BluetoothDevice device, int status, int newState) {
            
            // Device has been connected - start service discovery
            if (newState == BluetoothProfile.STATE_CONNECTED && mBluetoothGatt != null) {                

                Log.d(TAG,"onConnectionStateChange (" + device.getAddress()+")");
                if(!mConnectedDevices.contains(device)) {                    
                    mConnectedDevices.add(device);                     
                }
                
                ProximityReporter pr = ProximityReporter.get(device.getAddress());
                if (pr == null) {
                    Log.d(TAG,"onConnectionStateChange (" + device.getAddress()+") - we don't care about this anymore... @@@");
                    return;
                }                
                
                if (pr != null) {
                    pr.isConnected = true;
                    Intent intent = new Intent();
                    intent.setAction(PROXIMITY_CONNECT);
                    intent.putExtra(BluetoothDevice.EXTRA_DEVICE, pr.address);
                    sendBroadcast(intent);
                }
                displayNotification(pr, true);
            }
            else if (newState == BluetoothProfile.STATE_DISCONNECTED&& mBluetoothGatt != null) {                
               
                if (device != null && device.getBondState()!= BluetoothDevice.BOND_BONDED) {
                    if (ProximityReporter.containsKey(device.getAddress())) {
                        Log.d(TAG, "onConnectionStateChange Device " + device.getAddress() + " unbonded - remove device" );
                        remove(device, true);
                    }
                }
                proximityOnDeviceDisconnected(device);  
            }                
        }
        
        @Override
        public void onServicesDiscovered(BluetoothDevice device, int status) { 
            Log.d(TAG,"onServicesDiscovered -- status " + status);
            if(status != 0) {
                 Log.d(TAG,"onServicesDiscovered is failed -- status " + status);
                 return;
            }
            
            BluetoothGattService linkLossService = mBluetoothGatt.getService(device, LINK_LOSS_SERVICE_UUID);  
            if(linkLossService != null) {
                Log.d(TAG,"onServicesDiscovered - linkLossService");
                
                final BluetoothGattCharacteristic alertLevelCharacteristic = linkLossService.getCharacteristic(ALERT_LEVEL_CHARACTERISTIC_UUID); 
                if(alertLevelCharacteristic == null) {
                    Log.d(TAG,"alertLevelCharacteristic - NULL");
                    return;
                }
                
                ProximityReporter pr = ProximityReporter.get(device.getAddress());
                byte[] value = { pr.remoteLinkLossAlertLevel };
                alertLevelCharacteristic.setValue(value);
                mBluetoothGatt.writeCharacteristic(alertLevelCharacteristic);                
              }  

          }

        @Override
        public void onAppRegistered(int status) {
              Log.d(TAG, "onAppRegistered() - status=" + status); 
              connectReporters();
        }

        @Override
        public void onScanResult(BluetoothDevice device, int rssi, byte[] scanRecord) {
            Log.d(TAG, "onScanResult() - device=" + device + ", rssi=" + rssi); 
            sendDeviceFoundIntent(device);             
        }
    };
    public void findme(String bdaddr) {
        
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(bdaddr);
        BluetoothGattService immAlertService = mBluetoothGatt.getService(device, IMMEDIATE_ALERT_SERVICE_UUID); 
        if(immAlertService == null) {
            Log.d(TAG, "immAlertService is null");
            return;
        }
       
            BluetoothGattCharacteristic immAlertCharacteristic = immAlertService.getCharacteristic(ALERT_LEVEL_CHARACTERISTIC_UUID);
            if(immAlertCharacteristic == null) {
                Log.d(TAG, "immAlertService is null");
                return;
            }
           
            byte[] value = { 0x02 }; //ALERT_HIGH_LEVEL
            immAlertCharacteristic.setValue(value);                       
            mBluetoothGatt.writeCharacteristic(immAlertCharacteristic);
            
            
    }

    

    public void writeLinkLossAlertLevel(BluetoothDevice device,byte alertLevel) {
        BluetoothGattService linkLossService = mBluetoothGatt.getService(device, LINK_LOSS_SERVICE_UUID);  
        if(linkLossService == null) {
            Log.d(TAG,"linkLossService is null");
            return;
        }    
        BluetoothGattCharacteristic alertLevelCharacteristic = linkLossService.getCharacteristic(ALERT_LEVEL_CHARACTERISTIC_UUID); 
        if(alertLevelCharacteristic == null) {
            Log.d(TAG,"alertLevelCharacteristic is null");
            return;
         }
        ProximityReporter pr = ProximityReporter.get(device.getAddress());
        byte[] value = { pr.remoteLinkLossAlertLevel };
        alertLevelCharacteristic.setValue(value);            
        mBluetoothGatt.writeCharacteristic(alertLevelCharacteristic);
            
    }

    public void writeLinkLossAlertLevel(ProximityReporter pr) {
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(pr.address);
        writeLinkLossAlertLevel(device, pr.remoteLinkLossAlertLevel);
    }

    private void discoverServices(BluetoothDevice device) {
        //Discover Services is calling with delay.
        final BluetoothDevice tempDevice = device;
        final Timer timer = new Timer();
        timer.schedule( new TimerTask(){
        public void run() {                 
           mBluetoothGatt.discoverServices(tempDevice); 
          }
         }, 5000);               
      }
                   

   
    

    @Override
    public void onCreate() {
        instance = this;
        Log.d(TAG, "onCreate() called");
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            public void onLocationChanged(Location newLocation) {
                boolean locationNeeded = false;
                long currentTime = System.currentTimeMillis();

                for (ProximityReporter pr: ProximityReporter.getAll()) {
                    if (pr.isConnected == false && (currentTime - pr.cachedDisconnectStartTime < 60*1000)) {
                        locationNeeded = true;
                        if (isNewLocationBetter(pr.cachedLocation, newLocation)) {
                            pr.cachedLocation =  newLocation;
                            pr.longitude = newLocation.getLongitude();
                            pr.latitude = newLocation.getLatitude();
                            pr.accuracy = newLocation.getAccuracy();
                            pr.timestamp = newLocation.getTime();
                            pr.update();
                        }
                    }

                    if (!locationNeeded) {
                        updatingLocation = false;
                        locationManager.removeUpdates(locationListener);
                    }
                }
            }

            public void onProviderDisabled(String provider) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }
        };

        ProximityReporter.initDatabase(this); 
        if (mBluetoothAdapter == null) {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) return;
        }

        if (mBluetoothGatt == null) {
             BluetoothGattAdapter.getProfileProxy(this, mProfileServiceListener, BluetoothGattAdapter.GATT);
        }        
        Log.d(TAG,"Registering receivers");
        registerReceiver(mBroadcastReceiver, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
        connectReporters();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy() called");
        if(mConnectedDevices != null && mConnectedDevices.size() > 0) {
            ArrayList<BluetoothDevice> mTempConnectedDevices = (ArrayList<BluetoothDevice>)mConnectedDevices.clone();
            for(int i=0; i<mTempConnectedDevices.size(); i++) {
                Log.d(TAG,"updates the notfication & UI");
                proximityOnDeviceDisconnected(mTempConnectedDevices.get(i));
            }
            mTempConnectedDevices.clear();
        }

        if(mConnectedDevices != null) {
            mConnectedDevices.clear();
        }
        unregisterReceiver(mBroadcastReceiver);
        ProximityReporter.closeDatabase();
        instance = null;

        if (mBluetoothAdapter != null && mBluetoothGatt != null) {
            BluetoothGattAdapter.closeProfileProxy(BluetoothGattAdapter.GATT, mBluetoothGatt);
        }

        super.onDestroy();
    }

    public void connectReporters() {
        for (ProximityReporter pr: ProximityReporter.getAll()) {
            
            if(pr.isConnected == false) {
                BluetoothDevice bd = mBluetoothAdapter.getRemoteDevice(pr.address);                
                if(mBluetoothGatt != null) {
                    mBluetoothGatt.connect(bd, true);
                }
            }
        }
    }

    public boolean isAddressInDatabase(String bdaddr) {
        return ProximityReporter.containsKey(bdaddr);
    }

    public ProximityReporter getProximityReporter(String bdaddr) {
        return ProximityReporter.get(bdaddr);
    }

    public ProximityReporter createProximityReporter(String bdaddr) {
        ProximityReporter result = new ProximityReporter(bdaddr);
        result.insert();
        BluetoothDevice bd = mBluetoothAdapter.getRemoteDevice(bdaddr);        
        mBluetoothGatt.connect(bd, true);
        

        Intent intent = new Intent();
        intent.setAction(PROXIMITY_INSERT);
        intent.putExtra(BluetoothDevice.EXTRA_DEVICE, bdaddr);
        sendBroadcast(intent);

        return result;
    }
    
    public void update(ProximityReporter pr, boolean broadcast, boolean writeToDatabase) {
        if (writeToDatabase) {
            pr.update();
        }
        if (broadcast) {
            Intent intent = new Intent();
            intent.setAction(PROXIMITY_UPDATE);
            intent.putExtra(BluetoothDevice.EXTRA_DEVICE, pr.address);
            sendBroadcast(intent);
        }
    }

    public void remove(BluetoothDevice device, boolean broadcast) {
       
        ProximityReporter.delete(device.getAddress());
        try {
            mBluetoothGatt.cancelConnection(device);
        } catch (Exception ex) {
            Log.e(TAG, "cancelConnection() - " + ex.toString());
         
        }
        if (broadcast) {
            Intent intent = new Intent();
            intent.setAction(PROXIMITY_DELETE);
            intent.putExtra(BluetoothDevice.EXTRA_DEVICE, device.getAddress());
            sendBroadcast(intent);
        }
    }

    public void enable(ProximityReporter pr) {        
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(pr.address);
        if (device != null) {
            mBluetoothGatt.connect(device, true);
        }
    }

    public void disable(ProximityReporter pr) {
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(pr.address);
        if (device != null) {
            try {
                mBluetoothGatt.cancelConnection(device);
            } catch (Exception ex) {
                Log.e(TAG, "cancelConnection() - " + ex.toString());
            }            
        }        
    }

    private boolean isNewLocationBetter (Location currentLocation, Location newLocation) {
        if (currentLocation == null)
            return true;

        if (currentLocation.getTime() < newLocation.getTime() - 30*1000)
            return true; // currentLocation is too old;
        if (newLocation.getTime() < currentLocation.getTime() - 30*1000)
            return false; // newLocation is too old;

        if (currentLocation.hasAccuracy() && ! newLocation.hasAccuracy())
            return false; // current location is more accurate
        if (newLocation.hasAccuracy() && ! currentLocation.hasAccuracy())
            return true; // new location is more accurate

        if (newLocation.getAccuracy() < currentLocation.getAccuracy())
            return true;
        if (currentLocation.getAccuracy() < newLocation.getAccuracy())
            return false;

        // same accuracy - return newest result
        return (newLocation.getTime() > currentLocation.getTime());

    }

    private void displayNotification(ProximityReporter pr, boolean connected) {

        CharSequence contentTitle = getString(connected ? R.string.notification_title_connected : R.string.notification_title_disconnected);
        String bodyPrefix = getString(connected ? R.string.notification_body_prefix_connected : R.string.notification_body_prefix_disconnected);
        String bodySuffix = getString(connected ? R.string.notification_body_suffix_connected : R.string.notification_body_suffix_disconnected);
        String deviceName = pr.getName();
        String tickerText = bodyPrefix + deviceName + bodySuffix;
        SpannableString body = new SpannableString(tickerText);
        body.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), bodyPrefix.length(), bodyPrefix.length() + deviceName.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);

        int icon = R.drawable.notification_icon;
        long when = System.currentTimeMillis();
        Notification notification = new Notification(icon, tickerText, when);

        Intent notificationIntent = new Intent(this, ProximityMonitorActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        notification.setLatestEventInfo(this, contentTitle, body, contentIntent);

        if (pr.localLinkLossAlertVibrate)
            notification.defaults |= Notification.DEFAULT_VIBRATE;

        if (pr.localLinkLossAlertSound)
            notification.sound = Uri.parse(pr.localLinkLossAlertSoundUri);

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationID = addressToID(pr.address);
        nm.notify(notificationID, notification);
    }

    private void cancelNotification(String address) {
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationID = addressToID(address);
        nm.cancel(notificationID);
    }

    private int addressToID(String address) {
        String s = address.replace(":","");
        long l = Long.parseLong(s, 16);
        return (int) l;
    }

    public void scan(boolean start) {
        if (mBluetoothGatt == null) return;

        if (start) {
            mBluetoothGatt.startScan();
        } else {
            mBluetoothGatt.stopScan();
        }
    }

    private void sendDeviceFoundIntent(BluetoothDevice device) {
        Intent intent = new Intent(LE_DEVICE_FOUND);
        intent.putExtra(EXTRA_DEVICE, device);
        sendBroadcast(intent);
    }
//on BT OFF, for updating the notification & UI ++
    private synchronized void proximityOnDeviceDisconnected(BluetoothDevice device) {
        
            Log.d(TAG,"proximityOnDeviceDisconnected(" + device.getAddress()+")");

            if(mConnectedDevices!= null && mConnectedDevices.contains(device)) {                
                mConnectedDevices.remove(device);
            }

            // Connection closed
            Intent intent = new Intent();
            intent.setAction(PROXIMITY_DISCONNECT);
            intent.putExtra(BluetoothDevice.EXTRA_DEVICE, device.getAddress());
            sendBroadcast(intent);

            ProximityReporter pr = ProximityReporter.get(device.getAddress());
            if (pr != null) {
                pr.isConnected = false;
                pr.cachedLocation = null;
                pr.longitude = 0;
                pr.latitude = 0;
                pr.accuracy = 0;
                pr.timestamp = 0;

                pr.cachedLocation = null;
                pr.cachedDisconnectStartTime = System.currentTimeMillis();

                pr.update();
                displayNotification(pr, false);
            }

            if (!updatingLocation) {
                try {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 1, locationListener, getMainLooper());
                    updatingLocation = true;
                } catch (IllegalArgumentException ex) {
                    // GPS_PROVIDER not available
                }
                try {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 1, locationListener, getMainLooper());
                    updatingLocation = true;
                } catch (IllegalArgumentException ex) {
                    // NETWORK_PROVIDER not available
                }
            }
        }


    
}

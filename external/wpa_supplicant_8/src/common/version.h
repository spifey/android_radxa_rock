#ifndef VERSION_H
#define VERSION_H

#ifndef VERSION_STR_POSTFIX
#define VERSION_STR_POSTFIX ""
#endif /* VERSION_STR_POSTFIX */

#define VERSION_STR "2.0-devel-v1.0(powered by rockchip)" VERSION_STR_POSTFIX

#endif /* VERSION_H */

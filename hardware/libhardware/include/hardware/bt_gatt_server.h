/*******************************************************************************
 *
 *  Copyright (C) 2009-2012 Broadcom Corporation
 *
 *  This program is the proprietary software of Broadcom Corporation and/or its
 *  licensors, and may only be used, duplicated, modified or distributed
 *  pursuant to the terms and conditions of a separate, written license
 *  agreement executed between you and Broadcom (an "Authorized License").
 *  Except as set forth in an Authorized License, Broadcom grants no license
 *  (express or implied), right to use, or waiver of any kind with respect to
 *  the Software, and Broadcom expressly reserves all rights in and to the
 *  Software and all intellectual property rights therein.
 *  IF YOU HAVE NO AUTHORIZED LICENSE, THEN YOU HAVE NO RIGHT TO USE THIS
 *  SOFTWARE IN ANY WAY, AND SHOULD IMMEDIATELY NOTIFY BROADCOM AND DISCONTINUE
 *  ALL USE OF THE SOFTWARE.
 *
 *  Except as expressly set forth in the Authorized License,
 *
 *  1.  This program, including its structure, sequence and organization,
 *      constitutes the valuable trade secrets of Broadcom, and you shall
 *      use all reasonable efforts to protect the confidentiality thereof,
 *      and to use this information only in connection with your use of
 *      Broadcom integrated circuit products.
 *
 *  2.  TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED
 *      "AS IS" AND WITH ALL FAULTS AND BROADCOM MAKES NO PROMISES,
 *      REPRESENTATIONS OR WARRANTIES, EITHER EXPRESS, IMPLIED, STATUTORY,
 *      OR OTHERWISE, WITH RESPECT TO THE SOFTWARE.  BROADCOM SPECIFICALLY
 *      DISCLAIMS ANY AND ALL IMPLIED WARRANTIES OF TITLE, MERCHANTABILITY,
 *      NONINFRINGEMENT, FITNESS FOR A PARTICULAR PURPOSE, LACK OF VIRUSES,
 *      ACCURACY OR COMPLETENESS, QUIET ENJOYMENT, QUIET POSSESSION OR
 *      CORRESPONDENCE TO DESCRIPTION. YOU ASSUME THE ENTIRE RISK ARISING OUT
 *      OF USE OR PERFORMANCE OF THE SOFTWARE.
 *
 *  3.  TO THE MAXIMUM EXTENT PERMITTED BY LAW, IN NO EVENT SHALL BROADCOM OR
 *      ITS LICENSORS BE LIABLE FOR
 *      (i)   CONSEQUENTIAL, INCIDENTAL, SPECIAL, INDIRECT, OR EXEMPLARY
 *            DAMAGES WHATSOEVER ARISING OUT OF OR IN ANY WAY RELATING TO
 *            YOUR USE OF OR INABILITY TO USE THE SOFTWARE EVEN IF BROADCOM
 *            HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES; OR
 *      (ii)  ANY AMOUNT IN EXCESS OF THE AMOUNT ACTUALLY PAID FOR THE
 *            SOFTWARE ITSELF OR U.S. $1, WHICHEVER IS GREATER. THESE
 *            LIMITATIONS SHALL APPLY NOTWITHSTANDING ANY FAILURE OF
 *            ESSENTIAL PURPOSE OF ANY LIMITED REMEDY.
 *
 ******************************************************************************/

#ifndef ANDROID_INCLUDE_BT_GATT_SERVER_H
#define ANDROID_INCLUDE_BT_GATT_SERVER_H

#include <stdint.h>
#include "bt_gatt_types.h"

__BEGIN_DECLS

/** GATT value type used in response to remote read requests */
typedef struct
{
    uint8_t           value[BTGATT_MAX_ATTR_LEN];
    uint16_t          handle;
    uint16_t          offset;
    uint16_t          len;
    uint8_t           auth_req;
} btgatt_value_t;

/** GATT remote read request response type */
typedef union
{
    btgatt_value_t attr_value;
    uint16_t            handle;
} btgatt_response_t;

/** BT-GATT Server callback structure. */

/** Callback invoked in response to register_server */
typedef void (*register_server_callback)(uint8_t status, uint8_t server_if,
                bt_uuid_t *app_uuid);

/** Callback indicating that a remote device has connected or been disconnected */
typedef void (*connection_callback)(uint16_t conn_id, uint8_t connected,
                                    bt_bdaddr_t *bda);

/** Callback invoked in response to create_service */
typedef void (*service_added_callback)(uint8_t status, uint8_t server_if,
                btgatt_srvc_id_t *srvc_id, uint16_t srvc_handle);

/** Callback indicating that an included service has been added to a service */
typedef void (*included_service_added_callback)(uint8_t status, uint8_t server_if,
                uint16_t srvc_handle, uint16_t incl_srvc_handle);

/** Callback invoked when a characteristic has been added to a service */
typedef void (*characteristic_added_callback)(uint8_t status, uint8_t server_if,
                bt_uuid_t *uuid, uint16_t srvc_handle, uint16_t char_handle);

/** Callback invoked when a descriptor has been added to a characteristic */
typedef void (*descriptor_added_callback)(uint8_t status, uint8_t server_if,
                bt_uuid_t *uuid, uint16_t srvc_handle, uint16_t descr_handle);

/** Callback invoked in response to start_service */
typedef void (*service_started_callback)(uint8_t status, uint8_t server_if,
                                         uint16_t srvc_handle);

/** Callback invoked in response to stop_service */
typedef void (*service_stopped_callback)(uint8_t status, uint8_t server_if,
                                         uint16_t srvc_handle);

/** Callback triggered when a service has been deleted */
typedef void (*service_deleted_callback)(uint8_t status, uint8_t server_if,
                                         uint16_t srvc_handle);

/**
 * Callback invoked when a remote device has requested to read a characteristic
 * or descriptor. The application must respond by calling send_response
 */
typedef void (*request_read_callback)(uint16_t conn_id, uint32_t trans_id,
                bt_bdaddr_t *bda, uint16_t attr_handle, uint16_t offset,
                uint8_t is_long);

/**
 * Callback invoked when a remote device has requested to write to a
 * characteristic or descriptor.
 */
typedef void (*request_write_callback)(uint16_t conn_id, uint32_t trans_id,
                bt_bdaddr_t *bda, uint16_t attr_handle, uint16_t offset,
                uint16_t length, uint8_t need_rsp, uint8_t is_prep,
                uint8_t* value);

/** Callback invoked when a previously prepared write is to be executed */
typedef void (*request_exec_write_callback)(uint16_t conn_id, uint32_t trans_id,
                bt_bdaddr_t *bda, uint8_t exec_write);

/**
 * Callback triggered in response to send_response if the remote device
 * sends a confirmation.
 */
typedef void (*response_confirmation_callback)(uint8_t status, uint16_t handle);

/** Callback indicating the remote device has requested an MTU change */
typedef void (*request_mtu_callback)(uint16_t conn_id, uint32_t trans_id,
                                     bt_bdaddr_t *bda, uint16_t mtu);

typedef struct {
    register_server_callback        register_server_cb;
    connection_callback             connection_cb;
    service_added_callback          service_added_cb;
    included_service_added_callback included_service_added_cb;
    characteristic_added_callback   characteristic_added_cb;
    descriptor_added_callback       descriptor_added_cb;
    service_started_callback        service_started_cb;
    service_stopped_callback        service_stopped_cb;
    service_deleted_callback        service_deleted_cb;
    request_read_callback           request_read_cb;
    request_write_callback          request_write_cb;
    request_exec_write_callback     request_exec_write_cb;
    response_confirmation_callback  response_confirmation_cb;
    request_mtu_callback            request_mtu_cb;
} btgatt_server_callbacks_t;

/** Represents the standard BT-GATT server interface. */
typedef struct {
    /** Registers a GATT server application with the stack */
    bt_status_t (*register_server)( bt_uuid_t *uuid );

    /** Unregister a server application from the stack */
    bt_status_t (*unregister_server)( uint8_t server_if );

    /** Create a connection to a remote peripheral */
    bt_status_t (*open)( uint8_t server_if, const bt_bdaddr_t *bd_addr,
                    uint8_t is_direct );

    /** Disconnect an established connection or cancel a pending one */
    bt_status_t (*close)( uint8_t server_if, const bt_bdaddr_t *bd_addr,
                    uint16_t conn_id );

    /** Create a new service */
    bt_status_t (*add_service)( uint8_t server_if, btgatt_srvc_id_t *srvc_id,
                    int8_t num_handles);

    /** Assign an included service to it's parent service */
    bt_status_t (*add_included_service)( uint8_t server_if,
                    uint16_t service_handle, uint16_t included_handle);

    /** Add a characteristic to a service */
    bt_status_t (*add_characteristic)( uint8_t server_if,
                    uint16_t service_handle, bt_uuid_t *uuid,
                    uint8_t properties, uint16_t permissions);

    /** Add a descriptor to a given service */
    bt_status_t (*add_descriptor)(uint8_t server_if, uint16_t service_handle,
                    bt_uuid_t *uuid, uint16_t permissions);

    /** Starts a local service */
    bt_status_t (*start_service)(uint8_t server_if, uint16_t service_handle,
                    uint8_t transport);

    /** Stops a local service */
    bt_status_t (*stop_service)(uint8_t server_if, uint16_t service_handle);

    /** Delete a local service */
    bt_status_t (*delete_service)(uint8_t server_if, uint16_t service_handle);

    /** Send value indication to a remote device */
    bt_status_t (*send_indication)(uint8_t server_if, uint16_t attribute_handle,
                    uint16_t conn_id, uint16_t len, uint8_t confirm,
                    char* p_value);

    /** Send a response to a read/write operation */
    bt_status_t (*send_response)(uint16_t conn_id, uint32_t trans_id,
                    uint8_t status, btgatt_response_t *response);
} btgatt_server_interface_t;

__END_DECLS

#endif /* ANDROID_INCLUDE_BT_GATT_CLIENT_H */
